﻿using RabbitMQ.Client;

namespace RabbitMQPublisher
{
    public class RabbitMQService
    {
        private readonly string _hostName = "192.168.24.129";
        private readonly string _userName = "user1";
        private readonly string _password = "password";


        public IConnection GetRabbitMQConnection()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory() // RabbitMQ hostuna bağlanmak için kullanılır..
            {
                // RabbitMQ'nun bağlantı kuracağı host'u tanımlıyoruz.
                HostName = _hostName,
                UserName = _userName,
                Password = _password,

                Port = AmqpTcpEndpoint.UseDefaultPort
            };

            return connectionFactory.CreateConnection();
        }
    }
}
//< add key = "RabbitMQAdress" value = "192.168.24.129" />

//      < add key = "RabbitMQUserName" value = "user2" />

//         < add key = "RabbitMQPassword" value = "user2" />
