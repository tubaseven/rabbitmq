﻿using RabbitMQ.Client;
using System;
using System.Text;

namespace RabbitMQPublisher
{
    public class Publisher
    {
        private readonly RabbitMQService _rabbitMQService;

        public Publisher(string queueName, string message)
        {
            _rabbitMQService = new RabbitMQService();

            using (var connection = _rabbitMQService.GetRabbitMQConnection()) //IConnection dan geliyor.Servisten dönende IConnection
            {
                using (var channel = connection.CreateModel()) 
                {
                    channel.QueueDeclare(queue: queueName,durable: false, exclusive: false,autoDelete: false, arguments: null); 

                    channel.BasicPublish("", queueName, null, Encoding.UTF8.GetBytes(message));
                    //BasicPublish() methodu “exchange” aslında mesajın alınıp bir veya daha fazla queue’ya konmasını sağlar.

                    Console.WriteLine("{0} queue'su üzerine, \"{1}\" mesajı yazıldı.", queueName, message);
                }
            }
        }
    }
}
