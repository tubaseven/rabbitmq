﻿using System;

namespace RabbitMQPublisher
{
    public class Program
    {
        private static readonly string _queueName = "RabbitMQ-TEST7";
        private static Publisher _publisher;

        static void Main(string[] args)
        {

            Console.WriteLine("Message ");
            string message = Console.ReadLine();

            _publisher = new Publisher(_queueName, message);
            Console.ReadKey();
        }
    }
}
