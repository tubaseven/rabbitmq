﻿using RabbitMQPublisher;
using System;

namespace RabbitMQConsumer
{
    public class Program
    {
        private static readonly string _queueName = "RabbitMQ-Test7";
        private static Publisher _publisher;
        private static Consumer _consumer;
        public static void Main(string[] args)
        {
            Console.WriteLine("----------------------KUYRUKTAKİ MESAJLAR ------------------------------ ");
            _consumer = new Consumer(_queueName);   // KUYRUKTAKİ TÜM MESAJLARI GÖSTERİR


            Console.WriteLine("-----------------------    ------------------------------ ");

            Console.WriteLine("Kaç mesaj giriceksiniz  ");
            int count = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("Mesaj giriniz:  ");
                string _message = Console.ReadLine();
                _publisher = new Publisher(_queueName, _message);

            }
            Console.ReadKey();

        }
    }
}
