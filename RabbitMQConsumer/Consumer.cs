﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQPublisher;
using System;
using System.Text;

namespace RabbitMQConsumer
{
    public class Consumer
    {
        private readonly RabbitMQService _rabbitMQService;
        private EventingBasicConsumer _consumer;
        public IModel _channel;
        public EventHandler<ShutdownEventArgs> ShutdownEvent;
        public EventHandler<ShutdownEventArgs> ConnectionShutdownEvent;

        public Consumer(string queueName)
        {
            _rabbitMQService = new RabbitMQService();

            using (var connection = _rabbitMQService.GetRabbitMQConnection())
            {
                using (var _channel = connection.CreateModel())
                {

                    _channel.QueueDeclare(queue: queueName, //her birini farklı bi channela atmak için isme göre 
                                          durable: false,
                                          exclusive: false,
                                          autoDelete: false,
                                          arguments: null);

                    _consumer = new EventingBasicConsumer(_channel);

                    _consumer.Shutdown += ShutdownEvent;

                    connection.ConnectionShutdown += ConnectionShutdownEvent;

                    _consumer.Received += (model, ea) =>   // "consumer.Received" event sayesinde ilgili queue sürekli bir dinleme modunda olacaktır.
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);

                        Console.WriteLine("{0} isimli queue üzerinden gelen mesaj: \"{1}\"", queueName, message);
                    };
                    _channel.BasicConsume(queue: queueName, autoAck: false, consumer: _consumer);
                    
                }
            }
        }
    }
}
